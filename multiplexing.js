const express = require('express');

const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const shell = require('shelljs');

http.listen(3000)

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/multiplexing.html");
});

var chat = io
    .of("/chat")
    .on("connection", function (socket) {
        socket.emit("message", {message : "Huy tran"});
        console.log("Chat on connection");
        // Gửi data tới tất cả socket kết nối tới chat
        chat.emit("message", {message: "Everyone connect in chat will get this message"});
        // Gửi lại data khi nhận được lệnh gửi "ferret"
        socket.on('ferret', function (name, fn) {
            fn('reply woot for : ' + name);
        });
        // Tất cả socket kết nối tới chat đều nhận được trừ người gửi
        socket.broadcast.emit("message", { message: "Everyone connect in chat will get this message exept sender" })

        socket.on('message', function (data) {
            console.log("This is message data receive from send function: " + data)
        });
    })

var news = io
    .of("/news")
    .on("connection", function (socket) {
        io.emit('this', { will: 'be received by everyone' });
        console.log("News on connection");
    })
    .on("disconnect", function (socket) {
        console.log("News on disconnection");
    })
