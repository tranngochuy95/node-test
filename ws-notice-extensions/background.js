var followChannel = {};
var initFollowChannel = {
    vina: false,
    nusa: false,
    yim: false,
    system: false,
}

var notice = io("http://192.168.1.118:3001/notice"),
    system = io("http://192.168.1.118:3001/system"),
    channel = io("http://192.168.1.118:3001/channel");

// system.on("push", function (data) {
//     console.log(data);
//     var opt = {
//         type: "basic",
//         title: "Hey Huy!",
//         contextMessage: "https://vinaresearch.net/public/",
//         message: data.toString(),
//         iconUrl: "images/ws_logo_128.png",
//     }
//     chrome.notifications.create(null, opt, function (notificationId) { id = notificationId })
// });
notice.on("noticeMessage", function (data) {
    var opt = {
        type: "basic",
        title: data.senderName + " said:",
        contextMessage: "https://vinaresearch.net/public/",
        message: data.message,
        iconUrl: "images/ws_logo_128.png",
    }
    chrome.notifications.create(null, opt, function (notificationId) { id = notificationId })
})

chrome.runtime.onInstalled.addListener(function () {
    // Enable page action
    new chrome.declarativeContent.ShowPageAction()
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
        chrome.declarativeContent.onPageChanged.addRules([{
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: { hostContains: '' },
            })
            ],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });

    chrome.storage.local.get(['followChannel'], function (result) {
        console.log(result.followChannel);
        if (result.followChannel) {
            followChannel = result.followChannel;

            if (followChannel.vina) {
                logChannel(channel, "vina");
            }
            if (followChannel.nusa) {
                logChannel(channel, "nusa");
            }
            if (followChannel.yim) {
                logChannel(channel, "yim");
            }
            if (followChannel.system) {
                logChannel(channel, "system");
            }
        } else {
            followChannel = initFollowChannel;
            chrome.storage.local.set({ followChannel: followChannel }, function () { });
        }
    });
});

function logChannel(socket, channelName) {
    channelLogData = {
        action: 1,
        channel: channelName,
    };
    socket.emit("logChannel", channelLogData, function (data) {
        var socketCacheID = null;
        chrome.storage.local.get(['socketCacheID'], function (result) {
            socketCacheID = result.socketCacheID;
            oldSocketID = socketCacheID;
            console.log(oldSocketID);
            if (oldSocketID != data) {
                chrome.storage.local.set({ socketCacheID: data }, function () { });
                channelUnLogData = {
                    action: 0,
                    channel: false,
                    socketID: oldSocketID,
                }
                socket.emit("logChannel", channelUnLogData, function (data) { });
            }
        });
    });
};

chrome.runtime.onStartup.addListener(function () {
    chrome.storage.local.get(['followChannel'], function (result) {
        if (result.followChannel) {
            followChannel = result.followChannel;
            if (followChannel.vina) {
                logChannel(channel, "vina");
            }
            if (followChannel.nusa) {
                logChannel(channel, "nusa");
            }
            if (followChannel.yim) {
                logChannel(channel, "yim");
            }
            if (followChannel.system) {
                logChannel(channel, "system");
            }
        } else {
            followChannel = initFollowChannel;
            chrome.storage.local.set({ followChannel: followChannel }, function () { });
        }
    });
});
chrome.runtime.onSuspend.addListener(function () { console.log("onSuspend") });
chrome.runtime.onSuspendCanceled.addListener(function () { console.log("onSuspendCanceled") });
chrome.runtime.onUpdateAvailable.addListener(function () { console.log("onUpdateAvailable") });
chrome.runtime.onBrowserUpdateAvailable.addListener(function () { console.log("onBrowserUpdateAvailable") });
chrome.runtime.onConnect.addListener(function () { console.log("onConnect") });
chrome.runtime.onConnectExternal.addListener(function () { console.log("onConnectExternal") });
chrome.runtime.onMessage.addListener(function () { console.log("onMessage") });
chrome.runtime.onMessageExternal.addListener(function () { console.log("onMessageExternal") });
chrome.runtime.onRestartRequired.addListener(function () { console.log("onRestartRequired") });
chrome.management.onUninstalled.addListener(function () {alert("123")});
