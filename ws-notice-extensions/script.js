var background = chrome.extension.getBackgroundPage();
notice  = background.notice;
system  = background.system;
channel = background.channel;

followChannel = background.followChannel;

$(document).ready(function () {
    inputMessage = $("#message");
    btnSend = $("#send");
    btnSend.click(function (e) {
        channelListArr = [];
        if (followChannel.vina) {
            channelListArr.push("vina");
        }

        if (followChannel.nusa) {
            channelListArr.push("nusa");
        }

        if (followChannel.yim) {
            channelListArr.push("yim");
        }

        message = inputMessage.val();
        senderName = "Huy Trần";
        channelList = channelListArr;
        messageObject = {
            message: message,
            senderName: senderName,
            channel: channelList,
        };
        notice.emit("notice", messageObject);
    });

    if (followChannel.vina) {
        $("#channel_vina").prop('checked', true);
    }

    if (followChannel.nusa) {
        $("#channel_nusa").prop('checked', true);
    }

    if (followChannel.yim) {
        $("#channel_yim").prop('checked', true);
    }

    if (followChannel.system) {
        $("#channel_server").prop('checked', true);
    }

    $(".channel-log").change(function () {
        channelName = $(this).val();
        if (this.checked) {
            action = 1;

            switch (channelName) {
                case "vina":
                    followChannel.vina      = true;
                    break;
                case "nusa":
                    followChannel.nusa      = true;
                    break;
                case "yim":
                    followChannel.yim       = true;
                    break;
                case "system":
                    followChannel.system    = true;
                    break;
            }
            chrome.storage.local.set({ followChannel: followChannel }, function () {});
        } else {
            action = 0;

            switch (channelName) {
                case "vina":
                    followChannel.vina      = false;
                    break;
                case "nusa":
                    followChannel.nusa      = false;
                    break;
                case "yim":
                    followChannel.yim       = false;
                    break;
                case "system":
                    followChannel.system    = false;
                    break;
            }
            chrome.storage.local.set({ followChannel: followChannel }, function () { });
        }
        channelLogData = {
            action: action,
            channel: channelName,
        }
        channel.emit("logChannel", channelLogData, function(){});
    });
});