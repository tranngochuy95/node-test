const express = require('express');

const app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

const shell = require('shelljs');

http.listen(3000)

// app.use('css', express.static('myapp/public'));
app.use('/images', express.static('images'));
// app.use('/images', express.static(path.join(__dirname, 'images')));

app.get('/', (req, res) => {
    res.send("On index")
});

io.on("connection", function (socket) {
    // console.log('a user connected 1');
})

app.get('/chat', function (req, res) {
    res.sendFile(__dirname + "/index.html");
});
app.get('/news', function (req, res) {
    res.send("On news page")
});

io.of("/notification").on('connection', function (socket) {
    var minutes = 1, the_interval = minutes * 60 * 100;
    setInterval(function () {
        result = checkRamsv();
        if (parseInt(result) > 80) {
            socket.volatile.emit("push", result)
        }
    }, the_interval);
});

function checkRamsv() {
    result = shell.exec('./check-ram.sh');
    return result;
}

chat = io.of("/chat").on('connection', function (socket) {
    console.log("Socket ID "+ socket.id + " connected!");
    socket.on("event", function (data) {
        // console.log(socket);
        message = { "message": "I reply for message " + data.message };
        // console.log(socket.id);
        // io.emit('push', { for: "Message for every one" });
        io.emit('this', { will: 'be received by everyone' });
        socket.emit("push", "received message: " + data.message);
        socket.broadcast.to(socket.id).emit('push', 'for your eyes only');
        result = shell.exec('./check-ram.sh');
        socket.emit("console-log", result);
        io.emit("console-log", { for: "Message for every one" });
    })
});

// while (1) {
//     setTimeout(checkRamsv, 30000);
// }

news = io.of("/news")
    .on("connection", function name(socket) {
        socket.emit("push", "On news pages");
    })

app.get('/public', (req, res) => {
    res.send("public page");
});

app.get(/huy/, (req, res) => {
    res.send('<a href="http://localhost:3000/public">Huy</a>');
});

app.get("/users/:userID/name/:name", (req, res) => {
    res.json(res.params);
});

app.get('/example/b', function (req, res, next) {
    console.log('the response will be sent by the next function ...')
    next()
}, function (req, res) {
    res.send('Hello from B!');
})

// app.listen(3000, function () { console.log('Example app listening on port 3000!') })