module.exports = class noticeObject {
    constructor(title = "W&S notice", message) {
        this.title      = title;
        this.message    = message;
    }

    obj() {
        return {
            title:      this.title,
            message:    this.message,
        };
    }

    setTitle(title) {
        this.title = title;
    }

    setTitle(message) {
        this.message = message;
    }
}