const express = require('express');
const app = express();

const noticeObject = require('./noticeObject.js');

var http = require('http').Server(app);
var io = require('socket.io')(http);

http.listen(3001)

app.get('/', (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

channelClient = {
    vina    : [],
    nusa    : [],
    yim     : [],
    system  : [],
}


var ram = io
    .of("/ram")
    .on("connection", function (socket) {
        // notice = new noticeObject("This is title", "This is message").obj();
        // socket.emit("notice", notice);
        notice = new noticeObject("This is title for socket ID", "This is message").obj();
        socket.emit("notice", notice);
        console.log(socket.id);
        // socket.to(socket.id).emit('notice', notice);
        socket.on("chat-message", function (data) {
            console.log(data.id);
            socket.to(data.id).emit('notice', { message: "receive ping from " + socket.id});
        });
        // io.sockets.connected(socket.id).emit('notice', notice);
        // io.sockets.connected[socket.id].emit('notice', notice);
        // socket.on("huymessage", function (msg) {
        //     notice = new noticeObject("This is title for socket ID", "This is message").obj();
        //     // socket.emit("notice", notice);
        //     console.log(socket.id);
        //     io.to(socket.id).emit('notice', notice);
        //     // console.log(msg);
        // });
    });

var notice = io
    .of("/notice")
    .on("connection", function (socket) {
        socket.on("notice", function (data) {
            console.log(data);
            message = data.message;
            channel = data.channel;
            senderName = data.senderName,
            socketIds = [];
            channel.forEach(channelName => {
                switch (channelName) {
                    case "vina":
                        channelClient.vina.forEach(clientId => {
                            socketIds.push(clientId);
                        });
                        break;
                    case "nusa":
                        channelClient.nusa.forEach(clientId => {
                            socketIds.push(clientId);
                        });
                        break;
                    case "yim":
                        channelClient.yim.forEach(clientId => {
                            socketIds.push(clientId);
                        });
                        break;
                    default:
                        break;
                }
            });
            noticeMessage = {
                senderName  : senderName,
                message     : message,
            }

            socketIdsUnique = [];
            socketIds.forEach(id => {
                if (socketIdsUnique.indexOf(id) == -1) socketIdsUnique.push(id);
            });
            console.log(socketIdsUnique);
            socketIdsUnique.forEach(id => {
                socket.to(id.replace("/channel", "/notice")).emit('noticeMessage', noticeMessage);
            });
        });
    });

var system = io
    .of("/system")
    .on("connection", function (socket) {
        var minutes = 1, the_interval = minutes * 60 * 100;
        setInterval(function () {
            result = 90;
            if (parseInt(result) > 80) {
                // socket.volatile.emit("push", result)
            }
        }, the_interval);
    });

var channel = io
    .of("/channel")
    .on("connection", function (socket) {
        socket.on("logChannel", function(data, fn) {
            action = data.action;
            channel = data.channel;
            socketID = data.socketID;
            switch (action) {
                case 0:
                    if (channel) {
                        rmID = socket.id;
                    } else {
                        console.log(socketID);
                        rmID = socketID;
                    }
                    switch (channel) {
                        case "vina":
                            channelClient.vina.arrRemove(rmID);
                            break;
                        case "nusa":
                            channelClient.nusa.arrRemove(rmID);
                            break;
                        case "yim":
                            channelClient.yim.arrRemove(rmID);
                            break;
                        case "system":
                            channelClient.system.arrRemove(rmID);
                            break;
                        default:
                            channelClient.vina.arrRemove(rmID);
                            channelClient.nusa.arrRemove(rmID);
                            channelClient.yim.arrRemove(rmID);
                            channelClient.system.arrRemove(rmID);
                            break
                    }
                    break;
                default:
                    switch (channel) {
                        case "vina":
                            channelClient.vina.push(socket.id);
                            break;
                        case "nusa":
                            channelClient.nusa.push(socket.id);
                            break;
                        case "yim":
                            channelClient.yim.push(socket.id);
                            break;
                        case "system":
                            channelClient.system.push(socket.id);
                            break;
                    }
                    break;
            }
            console.log(channelClient);
            fn(socket.id);
        })
    })
    .on("disconnect", function (socket) {
        console.log("disconnect");

        channelClient.vina.arrRemove(socket.id);

        channelClient.nusa.arrRemove(socket.id);

        channelClient.yim.arrRemove(socket.id);

        channelClient.system.arrRemove(socket.id);

    });

Array.prototype.arrRemove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};